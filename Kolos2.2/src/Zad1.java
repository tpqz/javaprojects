class Produkt {

    private String nazwa;
    public double cenaJednostkowa;
    public int ilosc;

    public Produkt(String nazwa, double cenaJednostkowa, int ilosc){
        this.nazwa = nazwa;
        this.cenaJednostkowa = cenaJednostkowa;
        this.ilosc = ilosc;
    }

    public double obliczWartosc(){
       double wartosc = cenaJednostkowa * ilosc;
        System.out.print("Wartość produktu "+ nazwa + " - " +wartosc + " zł" + " za "+ ilosc + " produkt/ów" + "\n");
        return wartosc;
    }

    public String toString() {
        return  "\n" +"Nazwa: " + nazwa + "\n" + "Cena: " +
                cenaJednostkowa + "\n" + "Ilość: " + ilosc;
    }
}

class ProduktPromocyjny extends Produkt{
    private int obnizka;

    public ProduktPromocyjny(String nazwa, double cenaJednostkowa, int ilosc, int obnizka){
        super(nazwa, cenaJednostkowa, ilosc);
    }

    public double obliczWartosc(){
        double wartoscPoObnizce = (cenaJednostkowa - obnizka) * ilosc;
        System.out.print("Cena produktu po obniżce: " + wartoscPoObnizce + " zł"+ " za "+ ilosc + " produkt/ów" + "\n");
        return wartoscPoObnizce;
    }
}

class Koszyk{

    Produkt[] produkty = new Produkt[4];

    public void dodajProduktDoKoszyka(Produkt a, int nr){
        System.out.print("Dodano do koszyka: " + a + "\n" + "Ilość w koszyku: " + nr + "\n" + "-------------------" + "\n" );
    }

    public String toString(){
        return "Koszyk";
    }
}

public class Zad1{
    public static void main(String[] args){

        ProduktPromocyjny kawa = new ProduktPromocyjny("Jacobs", 9.40, 1, 0);
        ProduktPromocyjny herbata = new ProduktPromocyjny("Lipton", 5.90, 1, 1);
        ProduktPromocyjny sok = new ProduktPromocyjny("Hortex", 3.20, 2, 0);

        Koszyk koszyk = new Koszyk();
        koszyk.dodajProduktDoKoszyka(kawa, 4);
        koszyk.dodajProduktDoKoszyka(herbata, 4);
        koszyk.dodajProduktDoKoszyka(sok, 4);

        kawa.obliczWartosc();
        herbata.obliczWartosc();
        sok.obliczWartosc();
    }
}
