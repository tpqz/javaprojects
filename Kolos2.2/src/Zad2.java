import com.sun.javafx.scene.traversal.ContainerTabOrder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Arc2D;
import java.util.LinkedList;
import java.util.Random;

class Container extends JPanel{

    JLabel Od = new JLabel("Od");
    JTextField OdText = new JTextField(5);

    JLabel Do = new JLabel("Do");
    JTextField DoText = new JTextField(5);

    JTextArea wynik = new JTextArea(5,20);

    JButton button = new JButton("Generuj");

    public Container(){

        SpringLayout layout = new SpringLayout();
        setLayout(layout);

        add(Od);
        add(OdText);

        layout.putConstraint(SpringLayout.WEST, Od, 10, SpringLayout.WEST, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, Od, 25, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, OdText, 25, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.WEST, OdText, 20, SpringLayout.EAST, Od);

        add(Do);
        add(DoText);

        layout.putConstraint(SpringLayout.WEST, Do, 110, SpringLayout.WEST, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, Do, 25, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, DoText, 25, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.WEST, DoText, 20, SpringLayout.EAST, Do);

        add(wynik);
        layout.putConstraint(SpringLayout.WEST, wynik, 5, SpringLayout.WEST, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, wynik, 60, SpringLayout.NORTH, new JLabel(""));

        add(button);
        layout.putConstraint(SpringLayout.WEST, button, 75, SpringLayout.WEST, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, button, 150, SpringLayout.NORTH, new JLabel(""));

        Event event = new Event();
        button.addActionListener(event);
    }

    private class Event implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            int dolna = Integer.parseInt(OdText.getText().trim());
            int gorna = Integer.parseInt(DoText.getText().trim());

            double wylosuj = Math.random() * gorna;

           if (wylosuj <= gorna && wylosuj >= dolna){
               double zaokroglone = Math.round(wylosuj);
               String tekst = Double.toString(zaokroglone);
               wynik.setText(tekst);

            }

        }
    }
}

public class Zad2{
    public static void main(String[] args){
        final JFrame okno = new JFrame("Liczby pseudolosowe");
        okno.add(new Container());
        okno.setVisible(true);
        okno.setSize(250,220);
        okno.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }
}
