import javafx.scene.control.Tab;
import javafx.scene.shape.StrokeLineCap;

interface Wlaczalny{
    boolean wlacz();
    boolean wylacz();
}

interface GPS{
    boolean wlaczGPS();
    boolean wylaczGPS();
    double[] pobierzWspolrzedne();
}

class Komputer{
    String procesor;

    public Komputer(String procesor) {
    }

    public void ustawProcesor(String procesor){
        this.procesor = procesor;
    }

    public String pobierzProcesor(String procesor){
        return procesor;
    }

    public String toString(){
        return "Komputer posiada procesor marki: " + procesor;
    }
}

class Tablet extends Komputer implements Wlaczalny, GPS {
    private String nazwa;
    private double waga;
    private int jasnoscEkranu;
    private static int liczbaTabletow;

    public Tablet(String procesor, String nazwa, double waga){
        super(procesor);
        this.procesor = procesor;
        this.nazwa = nazwa;
        this.waga = waga;
    }

    public Tablet(String procesor, String nazwa){
        super(procesor);
        this.nazwa = nazwa;
    }

    private boolean resetuj(){
        System.out.println("Resetuję urządzenie...");
        return true;
    }

    public boolean ustawJasnoscEkranu(int jasnoscEkranu){
        System.out.println("Jasnosc ekranu ustawiona na: " + jasnoscEkranu);
        return true;
    }

    public boolean obroc(){
        System.out.println("Obracam ekran o 90 stopni");
        return true;
    }

    public static int pobierzLiczbeTabletow(int liczbaTabletow){
        return liczbaTabletow;
    }

    public String toString(){
        return "Tablet marki " + nazwa + "\n" +
                "Procesor: " + procesor + "\n" +
                "Waga: " + waga + "\n";

    }

    public boolean wlacz() {
        System.out.print("Wlaczam urzadzenie...");
        return true;
    }

    public boolean wylacz() {
        System.out.print("Wylaczam urzadzenie...");
        return false;
    }

    public boolean wlaczGPS() {
        System.out.print("Wlaczam GPS...");
        return true;
    }

    public boolean wylaczGPS() {
        System.out.print("Wlaczam GPS...");
        return false;
    }

    public double[] pobierzWspolrzedne() {
        System.out.print("Pobieram wspolrzedne...");
        return new double[4];
    }
}

public class KolosStary1{
    public static void main(String[] args){
        Tablet pierwszy = new Tablet("AMD", "LG", 12.3);
        Tablet drugi = new Tablet("Intel", "Samsung");
        Tablet trzeci = new Tablet("Qualcomm", "Google");
        System.out.println(pierwszy);
        trzeci.wlaczGPS();
        pierwszy.obroc();
        drugi.ustawJasnoscEkranu(100);

    }
}
