package com.company;

/**
 * Created by Mateusz on 09.03.2016.
 */

public class Value {
    private Integer id;
    private String joke;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJoke() {
        return joke;
    }

    public void setJoke(String joke) {
        this.joke = joke;
    }
}