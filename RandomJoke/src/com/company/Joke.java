package com.company;
import retrofit2.*;


/**
 * Created by Mateusz on 09.03.2016.
 */

public class Joke {
    private String type;
    private Value value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }
}