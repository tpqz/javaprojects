import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Container extends JPanel{

    Component usda = new JLabel("Dolar");
    Component usdb = new JTextField(5);

    Component eura = new JLabel("Euro");
    Component eurb = new JTextField(5);

    Component plna = new JLabel("Złoty");
    Component plnb = new JTextField(5);

    JButton button;
    public Container() {

        SpringLayout layout = new SpringLayout();
        setLayout(layout);

        add(usda);
        add(usdb);

        layout.putConstraint(SpringLayout.WEST, usda, 10, SpringLayout.WEST, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, usda, 25, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, usdb, 25, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.WEST, usdb, 20, SpringLayout.EAST, usda);

        add(eura);
        add(eurb);

        layout.putConstraint(SpringLayout.WEST, eura, 10, SpringLayout.WEST, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, eura, 50, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, eurb, 50, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.WEST, eurb, 24, SpringLayout.EAST, eura);

        add(plna);
        add(plnb);

        layout.putConstraint(SpringLayout.WEST, plna, 10, SpringLayout.WEST, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, plna, 75, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.NORTH, plnb, 75, SpringLayout.NORTH, new JLabel(""));
        layout.putConstraint(SpringLayout.WEST, plnb, 23, SpringLayout.EAST, plna);

        button = new JButton("Wylicz");
        add(button);
        layout.putConstraint(SpringLayout.SOUTH, button, 135, SpringLayout.SOUTH, new JLabel(""));
        layout.putConstraint(SpringLayout.WEST, button, 35, SpringLayout.WEST, new JLabel(""));
    }

    private class Event implements ActionListener {
            public void actionPerformed(ActionEvent event) {
            }
        }
    }


class Window extends JFrame{
    public Window(){

        setTitle("Konwerter Walut");
        setVisible(true);
        setSize(150,210);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setResizable(true);

    }
}

public class KonwerterWalut {
    public static void main(String[] args){

        Window okno = new Window();
        Container kontener = new Container();
        okno.add(kontener);
    }
}

