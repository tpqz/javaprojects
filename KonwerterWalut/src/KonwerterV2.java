import java.awt.*;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class KonwerterV2 {

    public static void main(String args[]) {

        JFrame frame = new JFrame("Konwerter walut");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container contentPane = frame.getContentPane();

        SpringLayout layout = new SpringLayout();
        contentPane.setLayout(layout);

        Component usda = new JLabel("Dolar");
        Component usdb = new JTextField(5);

        Component eura = new JLabel("Euro");
        Component eurb = new JTextField(5);

        Component plna = new JLabel("Złoty");
        Component plnb = new JTextField(5);

        contentPane.add(usda);
        contentPane.add(usdb);

        layout.putConstraint(SpringLayout.WEST, usda, 10, SpringLayout.WEST, contentPane);
        layout.putConstraint(SpringLayout.NORTH, usda, 25, SpringLayout.NORTH, contentPane);
        layout.putConstraint(SpringLayout.NORTH, usdb, 25, SpringLayout.NORTH, contentPane);
        layout.putConstraint(SpringLayout.WEST, usdb, 20, SpringLayout.EAST, usda);

        contentPane.add(eura);
        contentPane.add(eurb);

        layout.putConstraint(SpringLayout.WEST, eura, 10, SpringLayout.WEST, contentPane);
        layout.putConstraint(SpringLayout.NORTH, eura, 50, SpringLayout.NORTH, contentPane);
        layout.putConstraint(SpringLayout.NORTH, eurb, 50, SpringLayout.NORTH, contentPane);
        layout.putConstraint(SpringLayout.WEST, eurb, 24, SpringLayout.EAST, eura);

        contentPane.add(plna);
        contentPane.add(plnb);

        layout.putConstraint(SpringLayout.WEST, plna, 10, SpringLayout.WEST, contentPane);
        layout.putConstraint(SpringLayout.NORTH, plna, 75, SpringLayout.NORTH, contentPane);
        layout.putConstraint(SpringLayout.NORTH, plnb, 75, SpringLayout.NORTH, contentPane);
        layout.putConstraint(SpringLayout.WEST, plnb, 23, SpringLayout.EAST, plna);

        JButton button = new JButton("Wylicz");
        contentPane.add(button);
        layout.putConstraint(SpringLayout.SOUTH, button, 135, SpringLayout.SOUTH, new JLabel(""));
        layout.putConstraint(SpringLayout.WEST, button, 35, SpringLayout.WEST, contentPane);


        frame.setSize(150, 210);
        frame.setVisible(true);
        frame.setResizable(false);
    }


}