import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

import java.awt.font.NumericShaper;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.IntStream;

/**
 * Created by Mateusz on 19.07.2016.
 */
public class NameGenerator {
    public static void main(String[] args) {

        int length;

        String[] spolgloski = {"a", "e", "i", "o", "u", "y"};
        String[] samogloski = {"q", "w", "r", "t", "p", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "m"};

        System.out.print("Podaj dlugosc nicku: ");

        Scanner input = new Scanner(System.in);
        length = input.nextInt();

        for (int i = 0; i <= length; i++) {

            double rnd = Math.random() * 20;
            double rnd2 = Math.random() * 6;
            int random = (int) rnd;
            int random2 = (int) rnd2;

            if (i == 0) {
                System.out.print(samogloski[5]);
                System.out.print(samogloski[9]);
            }

            if (i > 1 && i % 2 == 0) {
                System.out.print(samogloski[random]);
            } else {
                System.out.print(spolgloski[random2]);
            }
        }
    }
}
