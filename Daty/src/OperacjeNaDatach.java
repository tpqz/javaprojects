/**
 * Nazwa: OperacjeNaDatach.java
 * Autor: Mateusz Tapa
 * Grupa: KrDZIs2015
 * Data:  30/11/2015
 *
 * Opis:  Program wyświetla liczbę dni które upłynęły od urodzenia,
 * dzień tygodnia narodzin, znak zodiaku, oraz ile dni pozostało do końca roku kalendarzowego.
 *
 *
 * W programie JCreator aby skompilować i uruchomić program należy użyć opcji Build File a następnie Run File.
 * Za pomocą konsoli:
 * Kompilacja: javac OperacjeNaDatach.java
 * Uruchomienie: java OperacjeNaDatach
 */


import java.util.*;

class inputDate {
    int day;
    int month;
    int year;

    public inputDate(int year, int month, int day){ // konstruktor klasy inputDate
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public static int subtract(inputDate dateOne, inputDate dateTwo){ // metoda wyliczająca liczbę dni pomiędzy dwoma datami

        Calendar first = new GregorianCalendar(dateOne.year, dateOne.month - 1, dateOne.day);
        Calendar second = new GregorianCalendar(dateTwo.year, dateTwo.month - 1, dateTwo.day);
        int days = (int) ((first.getTimeInMillis() - second.getTimeInMillis())/1000/3600/24);
        return days;
    }

    public static String getZodiac(inputDate dateOne) { // metoda ustalająca znak zodiaku dla podanej daty

        String zodiac = null;

        if(dateOne.month == 1 && dateOne.day >= 20 || dateOne.month == 2 && dateOne.day <= 18) {
            zodiac = "Wodnik";
        }
        if(dateOne.month == 2 && dateOne.day >= 19 || dateOne.month == 3 && dateOne.day <= 20) {
            zodiac = "Ryby";
        }
        if(dateOne.month == 3 && dateOne.day >= 21 || dateOne.month == 4 && dateOne.day <= 19) {
            zodiac = "Baran";
        }
        if(dateOne.month == 4 && dateOne.day >= 20 || dateOne.month == 5 && dateOne.day <= 20) {
            zodiac = "Byk";
        }
        if(dateOne.month == 5 && dateOne.day >= 21 || dateOne.month == 6 && dateOne.day <= 21) {
            zodiac = "Bliznieta";
        }
        if(dateOne.month == 6 && dateOne.day >= 22 || dateOne.month == 7 && dateOne.day <= 22) {
            zodiac = "Rak";
        }
        if(dateOne.month == 7 && dateOne.day >= 23 || dateOne.month == 8 && dateOne.day <= 22) {
            zodiac = "Lew";
        }
        if(dateOne.month == 8 && dateOne.day >= 23 || dateOne.month == 9 && dateOne.day <= 22) {
            zodiac = "Panna";
        }
        if(dateOne.month == 9 && dateOne.day >= 23 || dateOne.month == 10 && dateOne.day <= 22) {
            zodiac = "Waga";
        }
        if(dateOne.month == 10 && dateOne.day >= 23 || dateOne.month == 11 && dateOne.day <= 21) {
            zodiac = "Skorpion";
        }
        if(dateOne.month == 11 && dateOne.day >= 22 || dateOne.month == 12 && dateOne.day <= 21) {
            zodiac = "Strzelec";
        }
        if(dateOne.month == 12 && dateOne.day >= 22 || dateOne.month == 1 && dateOne.day <= 19) {
            zodiac = "Koziorozec";
        }
        return zodiac;

    }

    public static String dayOfWeek(inputDate dateOne){ // metoda ustalająca dzień tygodnia dla podanej daty

        Calendar day = new GregorianCalendar(dateOne.year, dateOne.month - 1, dateOne.day);
        int dayOf = day.get(Calendar.DAY_OF_WEEK);
            switch (dayOf){

                case 2:
                    return "Poniedzialek";
                case 3:
                    return "Wtorek";
                case 4:
                    return "Sroda";
                case 5:
                    return "Czwartek";
                case 6:
                    return "Piatek";
                case 0:
                    return "Sobota";
                case 1:
                    return "Niedziela";
                default:
                    return null;
            }
    }
}

public class OperacjeNaDatach {

    public static void main(String[] args) {

        inputDate birth = new inputDate(1995,1,26);
        inputDate actual = new inputDate(2015,11,30);
        inputDate endOfYear = new inputDate(2015,12,31);
        System.out.println("Przeżyłeś " + inputDate.subtract(actual, birth) + " dni");
        System.out.println("Do końca roku zostało " + inputDate.subtract(endOfYear, actual) + " dni");
        System.out.println("Urodziłeś się w " + inputDate.dayOfWeek(birth));
        System.out.println("Dziś jest " + inputDate.dayOfWeek(actual));
        System.out.println("Koniec roku nadejdzie w " + inputDate.dayOfWeek(endOfYear));
        System.out.println("Twoj znak zodiaku to " + inputDate.getZodiac(birth));
        System.out.println("Urodzeni dziś otrzymają " + inputDate.getZodiac(actual) + " jako znak zodiaku");

    }
}
