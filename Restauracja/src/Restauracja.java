import java.util.*;



class Hall{

    private Guest[] guests;
    private Table[] tables;
    private Televisor tv;
    private Snooker snooker;

    public Hall(int guestsQuant, int tableQuant, String tvChannel, boolean snookerTable){
        this.guests = new Guest[guestsQuant];
        this.tables = new Table[tableQuant];
        this.tv = new Televisor(tvChannel);
        this.snooker = new Snooker(snookerTable);
    }

    public Hall(int guestsQuant, int tableQuant, String tvChannel){
        this.guests = new Guest[guestsQuant];
        this.tables = new Table[tableQuant];
        this.tv = new Televisor(tvChannel);
    }

    public void getGuests() {
        System.out.println(" W tej sali aktualnie jest " + this.guests.length + " gosci");
    }

    public void getTables(){
        int freetables = this.tables.length - this.guests.length;
        System.out.println("W tej sali aktualnie jest " + freetables + " wolnych miejsc");
    }

    public void getTv(){
        System.out.println("W telewizorze aktualnie nadaje " + this.tv.getChannelOne());
    }

    public void getTvTwo(){
        System.out.println("W telewizorze aktualnie nadaje " + this.tv.getChannelTwo());
    }

    public void getSnookerTable(){
        snooker.getSnooker();

    }
}

public class Restauracja {
    private Hall[] hallType;


    public Restauracja(int hallQuant) {
        this.hallType = new Hall[hallQuant];
    }

    public void showCondition() {

            System.out.println("Sala dla niepalacych");
            this.hallType[0].getGuests();
            this.hallType[0].getTables();
            this.hallType[0].getTv();
            System.out.println();

            System.out.println("Sala dla palacych");
            this.hallType[1].getGuests();
            this.hallType[1].getTables();
            this.hallType[1].getTvTwo();
            this.hallType[1].getSnookerTable();
            System.out.println();
    }
    public static void main(String[] args){

        Restauracja restaurant = new Restauracja(2);
        restaurant.hallType[0] = new Hall(32,48,"Discovery");
        restaurant.hallType[1] = new Hall(19,32,"NatGeo", false);

        restaurant.showCondition();

    }
}