interface Wyswietlaj{
    boolean wyswietl(String a);
}

class Ekran implements Wyswietlaj{
    private String nazwa;
    private String model;
    public static int iloscEkranow;

    public Ekran(String nazwa){
        this.nazwa = nazwa;
    }

    public Ekran(String nazwa, String model){
        this.nazwa = nazwa;
        this.model = model;
    }

    public boolean ustawNazwe(String nazwa){
        return true;
    }

    public String pobierzNazwe(){
        System.out.print("Nazwa: " + nazwa);
        return nazwa;
    }

    public boolean ustawModel(String model){
        return true;
    }

    public static int pobierzLiczbeEkranowLCD(){
        return iloscEkranow;
    }

    private boolean wykonajKalibracje(){
        System.out.print("Kalibracja wykonana poprawnie...");
        return true;
    }

    public String toString(){
        return nazwa + model;
    }

    public boolean wyswietl(String s){
        System.out.print(s);
        return true;
    }
}

public class EkranLCD {

    public static void main(String[] args) {

        Ekran ekran = new Ekran("LG", "VG240");

    }
}
