abstract class Char {
    private String name;
    private String race;
    private String profession;
    protected int level;
    protected int magicLevel;
    protected int distanceLevel;
    protected int meeleLevel;

    public Char(String name, String race, String profession) {
        this.name = name;
        this.race = race;
        this.profession = profession;
    }
}

class Knight extends Char{

    public Knight(String name, String race, String profession){
            super(name, race, profession);
            this.level = 1;
            this.magicLevel = 0;
            this.distanceLevel = 0;
            this.meeleLevel = 10;
        }

        public void getLevel(){
            System.out.println("Poziom postaci: " + level + "\n");
        }

        public void getMLevel(){
            System.out.println("Poziom magiczny postaci: " + magicLevel);
        }

        public void getDistLevel(){
            System.out.println("Poziom dystansowy postaci: " + distanceLevel);
        }

        public void getMeeleLevel(){
            System.out.println("Poziom walki wręcz postaci: " + meeleLevel);
        }

        public int levelUp(int lvl){
            level = lvl + 1;
            System.out.print( "Awans! Poziom postaci: " + level + "\n");
            return level;
        }
    }
public class Game {

    public static void main(String[] args) {

        Knight mati = new Knight("Mati", "Human", "Knight");
        mati.getLevel();
        mati.levelUp(11);
        mati.getLevel();
    }
}
