interface nadajAlbum{
    void nadajNrAlbumu();
}

 class Osoba{
     private String nazwisko;
     private String imie;
     private String pesel;

    public Osoba(String nazwisko, String imie, String pesel){
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.pesel = pesel;
    }

    public String toString(){
        return  "Imię: " + imie + "\n" +
                "Nazwisko: " + nazwisko + "\n" +
                "PESEL: " + pesel;
    }
}

class Student extends Osoba implements nadajAlbum{
    private int nrAlbumu;
    private String kierunekStudiow;
    private boolean semestrZaliczony;
    private int kolejnyNrAlbumu;

    public Student(String nazwisko, String imie, String pesel){
        super(nazwisko, imie, pesel);
    }

    public String pobierzKierunekStudiow(){
        return kierunekStudiow;
    }

    public void ustawKierunekStudiow(String kierunekStudiow){
        this.kierunekStudiow = kierunekStudiow;
        System.out.print("Kierunek: " + kierunekStudiow);
    }

    public void zaliczSemestr(){
        if (semestrZaliczony == true )
            System.out.print("Semestr zaliczony");
        else
            System.out.print("Semestr niezaliczony");
    }

    public void nadajNrAlbumu(){
        this.nrAlbumu = nrAlbumu;
    }
}


public class KolosZad11 {
    public static void main(String[] args){

        Student studentOne = new Student("Tapa", "Mateusz", "95012607554");
        System.out.print(studentOne + "\n");
        studentOne.ustawKierunekStudiow("Informatyka" + "\n");
        studentOne.zaliczSemestr();

    }
}
