import java.util.Scanner;

public class PoprawnaLiczbaNaturalna {
    public static void main(String[] args) {

        System.out.println("Podaj liczbę naturalną: ");
        Scanner inputIn = new Scanner(System.in);
        String input = inputIn.nextLine();

        if(input.matches("[0-9]+")){
            System.out.println("OK");
        }else{
            System.out.println("ZLE");
        }
    }
}