/**
 * Created by Mateusz on 11.05.2016.
 */

import java.util.*;

class Student implements Comparable{
    private String imie;
    private String nazwisko;
    private int nrAlbumu;

    public Student(String imie, String nazwisko, int nrAlbumu){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.nrAlbumu = nrAlbumu;
    }

    @Override
    public int compareTo(Object o) {
        return (nrAlbumu - ((Student)o).nrAlbumu);
    }

    public String toString(){
        return "Imie: " + imie + ", Nazwisko: " + nazwisko + ", Nr albumu: " + nrAlbumu;
    }
}

public class ListaStudentow {
    public static void main(String[] args){

        Student[] lista = new Student[4];

        lista[0] = new Student ("Mateusz", "Tapa", 188478);
        lista[1] = new Student ("Jan", "Lol", 185123);
        lista[2] = new Student ("Stefan", "Tarzan", 115478);
        lista[3] = new Student ("Eldo", "Prezidento", 198478);

        Arrays.sort(lista);

        for(Student student:lista){
            System.out.println(student);
        }
    }
}
