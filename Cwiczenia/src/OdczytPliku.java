/**
 * Created by Mateusz on 11.05.2016.
 */

import java.io.*;

public class OdczytPliku {
    public static void main(String[] args) {

        FileReader plik = null;
        int znak;

        try {
            plik = new FileReader("dane.txt");

            while ((znak = plik.read()) != -1) {
                System.out.print((char) znak);
            }

        }catch (FileNotFoundException a) {
            System.out.print("Brak pliku");
        }

        catch(IOException e){
            System.out.println("Blad odczytu");
        }

        finally{
            if(plik != null){
                try {
                    plik.close();
                } catch (IOException e) {
                    System.out.print("Blad w zamknieciu pliku");
                }
            }
        }
    }
}