/**
 * Created by Mateusz on 11.05.2016.
 */

interface Wlacza{
    public void wlacz();
    public void wylacz();
    public boolean czyDziala();
}

interface Odtwarza{
    public void start();
    public void stop();
    public void pauza();
}

abstract class Urzadzenie implements Wlacza, Odtwarza{
    protected int glosnosc = 10;
    private boolean dziala;
    protected String marka;

    public Urzadzenie(String marka){
        this.marka = marka;
    }

    public void wlacz(){
        System.out.println("Wlaczam");
    }

    public void wylacz(){
        System.out.println("Wylaczam");
    }

    public boolean czyDziala() {
        if (dziala == true) {
            System.out.print("Dziala");
            return true;
        } else {
            System.out.print("Nie dziala");
            return false;
        }
    }
        public abstract void glosniej();
        public abstract void ciszej();
}


class OdtwarzaczCD extends Urzadzenie {

    public OdtwarzaczCD(String marka){
        super(marka);
    }

    public void glosniej(){
        ++glosnosc;
        System.out.println("Glosnosc zwiekszona o 1");
    }

    public void ciszej(){
        --glosnosc;
        System.out.println("Glosnosc zmniejszona o 1");
    }

    public void start(){
        System.out.println("Rozpoczynam odtwarzanie");
    }

    public void stop(){
        System.out.println("Zatrzymano odtwarzanie");
    }

    public void pauza(){
        System.out.println("Pauza");
    }

    public String toString(){
        return "Marka: " + marka + ", glosnosc: " + glosnosc;
    }
}

public class Odtwarzacz{
    public static void main(String[] args){

        OdtwarzaczCD sony = new OdtwarzaczCD("Sony");
        System.out.println(sony);
    }
}