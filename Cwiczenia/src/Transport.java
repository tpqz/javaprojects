/**
 * Created by Mateusz on 11.05.2016.
 */

interface Jezdzi {
    public void jedz();
}

interface Plywa {
    public void plyn();
}

interface Lata{
    public void lec();
}

class Samochod implements Jezdzi {
    public void jedz(){
        System.out.println("JADE");
    }
}

class Statek implements Plywa{
    public void plyn(){
        System.out.println("PLYNE");
    }
}

class Samolot implements Lata{
    public void lec(){
        System.out.println("LECE");
    }
}

public class Transport {
    public static void main ( String[] agrs){

        Samochod x = new Samochod();
        x.jedz();


    }
}
