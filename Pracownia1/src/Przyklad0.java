/*
 * Porownanie metod IterujA oraz IterujB dla iteracji prostej
 */

public class Przyklad0{
    int n;          //rozmiar macierzy
    double eps;     //zadaje wartosc epsilona do warunku stopu
    int norma;      //zadaje norme, ktora bedzie uzywana
    
    //Konstruktor
    public Przyklad0(int nn, int normaa, double epss) {
        n = nn;
        eps = epss;
        norma = normaa;
    }
    
    //Metoda sluzaca do porownania metod IterujA oraz IterujB
    public void Porownaj() {
        
        //zadaje uklad o rozmiarze n
        Uklad u1 = new Uklad(n);
        
        //losuje uklad symetryczny, dodatnio okreslony
        u1.LosujUkladSymetrycznyDodatnioOkreslony(0.1);
        
        //rozwiazuje uklad z wykorzystaniem metody iteracji prostej
        IteracjaProsta test1 = new IteracjaProsta(u1);
        IteracjaProsta test2 = new IteracjaProsta(u1);

        
        //przygotowuje macierze i wektory potrzebne do iteracji
        test1.Przygotuj();
        test2.Przygotuj();
        
        //wyswietlam normy macierzy
        System.out.println("Norma wierszowa: " + test1.NormaMacierzy(norma));
                
        //wykonuje iteracje do momentu, gdy norma roznicy kolejnych rozwiazan jest mniejsza 0.00001
        test1.IterujA(eps, norma);
        test1.SprawdzRozwiazanie(norma);
        test2.IterujB(eps, norma);
        test2.SprawdzRozwiazanie(norma);      
    }
    
}
