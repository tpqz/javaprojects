/*
 * Porownanie metod iteracji prostej i Seidela
 */

public class Przyklad1 {
    int n;          //rozmiar macierzy
    int k;          //numer pierwszej normy wektora, ktora bedzie wyswietlona na ekranie
    double eps;     //zadaje wartosc epsilona do warunku stopu
    int norma;      //zadaje norme, ktora bedzie uzywana
    
    //Konstruktor
    public Przyklad1(int nn, int normaa, double epss, int kk) {
        n = nn;
        k = kk;
        eps = epss;
        norma = normaa;
    }
    
    //Metoda porownujaca zbieznosc metod iteracji prostej i Seidela
    public void BadajZbieznosc() {
        
        //zadaje uklad o rozmiarze n
        Uklad u1 = new Uklad(n);
        
        //losuje uklad symetryczny, dodatnio okreslony
        u1.LosujUkladSymetrycznyDodatnioOkreslony(0.3);
        
        //rozwiazuje uklad z wykorzystaniem metody iteracji prostej i Seidela
        IteracjaProsta test1 = new IteracjaProsta(u1);
        IteracjaSeidela test2 = new IteracjaSeidela(u1);
        
        //przygotowuje macierze i wektory potrzebne do iteracji
        test1.Przygotuj();
        test2.Przygotuj();
        
        //wyswietlam normy macierzy
        System.out.println("Norma wierszowa: " + test1.NormaMacierzy(0));
        System.out.println("Norma kolumnowa: " + test1.NormaMacierzy(1));
        System.out.println("Norma euklidesowa: " + test1.NormaMacierzy(2));
        
        //wykonuje iteracje do momentu, gdy norma roznicy kolejnych rozwiazan jest mniejsza 0.00001
        test1.IterujA(eps, norma);
        test1.SprawdzRozwiazanie(norma);
        test2.IterujA(eps, norma);
        test2.SprawdzRozwiazanie(norma);
        
        //tworze wykres obrazujacy zbieznosc obu metod, rysuje normy wektorow od wybranego przyblizenia
        //pierwszy ciag norm - kolor czerwony, drugi ciag norm - kolor niebieski
        Wykresik wykresik = new Wykresik(test1.norma_x, test2.norma_x, k);        
    }
}
