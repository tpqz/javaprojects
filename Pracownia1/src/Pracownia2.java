/*
 * Klasa glowna - tutaj wolamy metody odpowiednich klas
 */

public class Pracownia2 {

    public static void main(String[] args) {
        
        /* S E K C J A  0
         * porownanie metod IterujA oraz IterujB */
        /*
        //tworze nowy obiekt klasy Przyklad0
        Przyklad0 test0 = new Przyklad0(100, 0, 1.0E-4);
        
        //badam zbieznosc obu metod
        test0.Porownaj();
        */
        
        /* S E K C J A  1
         * porownanie zbieznosci metod iteracji prostej i Seidela */
        /*
        //tworze nowy obiekt klasy Przyklad1
        Przyklad1 test1 = new Przyklad1(100, 2, 10E-9, 0);
        
        //badam zbieznosc obu metod
        test1.BadajZbieznosc();
        */
        
        /* S E K C J A  2
         * przyklad zbieznosci metod iteracyjnych dla macierzy symetrycznych, 
         * dodatnio okreslonych o normach wiekszych niz 1 */
        /* 
        //tworze nowy obiekt klasy Przyklad2
        Przyklad2 test2 = new Przyklad2(0, 100);
        
        //badam zbieznosc obu metod
        test2.BadajZbieznosc();
        */
        
        /* S E K C J A  3
         * przyklad badania zbieznosci metody iteracji 
         * - tutaj w zaleznosci od wektora poczatkowego */

        //tworze nowy obiekt klasy Przklad3
        Przyklad3 test3 = new Przyklad3(3.0);
        
        //badam zbieznosc metody dla wektora poczatkowego o zadanym rzedzie normy
        test3.BadajZbieznosc();

        
        /* S E K C J A  4
         * przyklad zastosowania metody potegowej */
        /*
        //tworze nowy obiekt klasy Przyklad4
        Przyklad4 test4 = new Przyklad4(5, 10E-8);
        
        //wyznaczam dominujaca wartosc wlasna metoda potegowa
        test4.WartoscWlasna();
        */
        
         /* S E K C J A  5
         * przyklad zastosowania iteracji Seidela do znalezienia rankingu stron */
        /*
        //tworze nowy obiekt klasy Przyklad5
        Przyklad5 test5 = new Przyklad5(10, 1, 50);
        
        //wyznaczam ranking stron wywolujac odpowiednia metode klasy Przyklad5
        test5.PageRankIteracja();   
        */
        
        /* S E K C J A 6
         * zadanie 1 */
        /*
         *tutaj mozna wpisac kod realizujacy zadanie 1
         * 
        */
        
        /* S E K C J A 7
         * zadanie 2 */
        /*
         *tutaj mozna wpisac kod realizujacy zadanie 2
         * 
        */
    }
}
