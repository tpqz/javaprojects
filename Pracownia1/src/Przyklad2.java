/*
 * Przyklad zbieznosci metod iteracji prostej i Seidela dla macierzy symetrycznej
 * i dodatnio okreslonej o normach wiekszych od 1
 * 
 */

public class Przyklad2 {
    int n = 3;          //rozmiar macierzy
    int norma;          //wybrana norma
    int iteracje;       //liczba iteracji
    
    //Konstruktor
    public Przyklad2(int normaa, int iteracjee) {
        norma = normaa;
        iteracje = iteracjee;
    }
    
    //Metoda badajaca zbieznosc zadanego ukladu
    public void BadajZbieznosc() {

        //zadaje uklad o rozmiarze n
        Uklad u1 = new Uklad(n);
        u1.A[0][0] = 3.0;
        u1.A[0][1] = 0.0;
        u1.A[0][2] = -5.0;
        u1.A[1][0] = 0.0;
        u1.A[1][1] = 8.0;
        u1.A[1][2] = 2.0;
        u1.A[2][0] = -5.0;
        u1.A[2][1] = 2.0;
        u1.A[2][2] = 9.0;
        u1.b[0] = 1.0;
        u1.b[1] = 1.0;
        u1.b[2] = 1.0;
        
        //wyswietlam uklad na konsoli
        u1.WyswietlUklad();
        
        //powoluje do istnienia obiekty klas IteracjaProsta oraz IteracjaSeidela
        IteracjaProsta test1 = new IteracjaProsta(u1);
        IteracjaSeidela test2 = new IteracjaSeidela(u1);
        
        //przygotowuje macierze i wektory potrzebne do iteracji
        test1.Przygotuj();
        test2.Przygotuj();
        
        //wyswietlam normy macierzy
        System.out.println("Norma wierszowa: " + test1.NormaMacierzy(0));
        System.out.println("Norma kolumnowa: " + test1.NormaMacierzy(1));
        System.out.println("Norma euklidesowa: " + test1.NormaMacierzy(2));
        
        //wykonuje zadana liczbe iteracji i sprawdzam rozwiazanie przyblizone
        test1.Iteruj(iteracje, norma);
        test1.SprawdzRozwiazanie(norma);
        test2.Iteruj(iteracje, norma);
        test2.SprawdzRozwiazanie(norma);
        
        //tworze wykres obrazujacy zbieznosc, rysuje normy wektorow
        Wykresik wykresik = new Wykresik(test1.norma_x, test2.norma_x, 0);
    }
}
