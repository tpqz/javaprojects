/*
 * Klasa, ktora mozna wykorzystac do zrealizowania zadania 1
 */

public class Zadanie2 {
    int n;          //rozmiar macierzy
    int k;          //liczba iteracji
    double eps;     //zadaje wartosc epsilona do warunku stopu
    double gamma;   //parametr gamma
    int norma;      //zadaje norme, ktora bedzie uzywana
    
    //Konstruktor
    public Zadanie2(int nn, int kk, double epss, double gammaa, int normaa) {
        n = nn;
        k = kk;
        eps = epss;
        gamma = gammaa;
        norma = normaa;
    }
    
    public void BadajZbieznosc(){
        
        //zadaje uklad o rozmiarze n
        PageRank P = new PageRank(n);
        
        //losuje macierz przejscia podajac parametr gamma - prawdopodobienstwo 
        //wystepowania linka ze strony j do strony i  
        P.Losuj(gamma);
        
        //wyznaczam ranking z wykorzystaniem metody iteracji prostej lub Seidela
        /* do uzupelnienia */
        
        //wyznaczam ranking z wykorzystaniem metody potegowej
        /* do uzupelnienia */
        
    }
}
