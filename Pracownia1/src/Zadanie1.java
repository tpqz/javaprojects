/*
 * Klasa, ktora mozna wykorzystac do zrealizowania zadania 1
 */

public class Zadanie1 {
    int n;          //rozmiar macierzy
    int k;          //liczba iteracji
    double eps;     //zadaje wartosc epsilona do warunku stopu
    double alfa;    //parametr alfa
    int norma;      //zadaje norme, ktora bedzie uzywana
    
    //Konstruktor
    public Zadanie1(int nn, int kk, double epss, double alfaa, int normaa) {
        n = nn;
        k = kk;
        eps = epss;
        alfa = alfaa;
        norma = normaa;
    }
    
    public void BadajZbieznosc(){
        
        //zadaje uklad o rozmiarze n
        Uklad u1 = new Uklad(n);
        
        //losuje uklad symetryczny, dodatnio okreslony z parametrem alfa
        u1.LosujUkladSymetrycznyDodatnioOkreslony(alfa);
        
        //rozwiazuje uklad z wykorzystaniem metody iteracji prostej lub Seidela
        /* do uzupelnienia */
        
        //przygotowuje macierze i wektory potrzebne do iteracji
        /* do uzupelnienia */
        
        //wyswietlam norme macierzy D
        /* do uzupelnienia */
        
        //wywoluje odpowiednia metode
        /* do uzupelnienia */
        
        //sprawdzam dokladnosc uzyskanego rozwiazania
        /* do uzupelnienia */
    }
}
