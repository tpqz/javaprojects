/*
 * Przyklad zastosowania metody potegowej
 */

public class Przyklad4 {
    int n;          //rozmiar macierzy
    double eps;     //parametr kryterium stopu
    
    //Konstruktor zadajacy rozmiar macierzy
    public Przyklad4(int nn, double epsilon) {
        n = nn;
        eps = epsilon;
    }
       
    //Metoda wyznaczajaca najwieksza wartosc wlasna i wektor wlasny
    public void WartoscWlasna() {
        
        //zadaje uklad o rozmiarze n
        Uklad u1 = new Uklad(n);
        
        //losuje dowolna macierz
        u1.LosujUklad();
        
        //tworze nowy obiekt klasy MetodaPotegowa
        MetodaPotegowa test = new MetodaPotegowa(u1); 
        
        //wykonuje metode potegowa do momentu, gdy kolejne wartosci wlasne 
        //beda sie roznily o mniej niz 1.0E-10
        test.IterujA(eps);
        
        //wyznaczam wykres poczawszy od 1 przyblizenia wartosci wlasnej
        Wykresik wykresik = new Wykresik(test.lambda,1);
        
        //wyswietlam rozwiazanie (wektor wlasny i wartosc wlasna)
        test.WyswietlRozwiazanie();
    }
}
