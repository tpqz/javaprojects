/*
 * Klasa realizujaca algorytm Google Page Rank z zastosowaniem iteracji Seidela 
 * do rozwiazania odpowiedniego ukladu rownan
 */

public class Przyklad5{
    int n;          //rozmiar macierzy
    int norma;      //norma, ktora bedzie uzywana
    int N;          //liczba iteracji
    
    //Konstruktor zadajacy podstawowe parametry
    public Przyklad5(int nn, int normaa, int NN) {
        n = nn;
        norma = normaa;
        N = NN;
    }
    
    //Metoda wyznaczajaca ranking stron z wykorzystaniem iteracji Seidela 
    public void PageRankIteracja(){
        
        //zadaje uklad o rozmiarze n
        PageRank P = new PageRank(n);
        
        //losuje macierz przejscia podajac parametr gamma - prawdopodobienstwo 
        //wystepowania linka ze strony j do strony i  
        P.Losuj(0.4);
        
        /* Rozwiazuje problem metoda iteracji Seidela wykonujac zadana liczbe iteracji
         * Najpierw odejmuje jedynki na przekatnej, potem usuwam ostatnie z rownan,
         * a ostatnia z niewiadomych przyjmuje za 1.0
         */
        P.PrzygotujDoIteracji();
        
        //powoluje do istnienia obiekt klasy IteracjaSeidela
        IteracjaSeidela test = new IteracjaSeidela(P.u);
        
        ///przygotowuje macierz D i wektor C potrzebne do iteracji
        test.Przygotuj();
        
        //wykonuje zadana liczbe iteracji
        test.Iteruj(N, norma);
        
        //wyswietlam rozwiazanie
        System.out.print("Wektor wlasny: ");
        test.WyswietlRozwiazanie();
        
        //wyswietlam blad rozwiazania
        test.SprawdzRozwiazanie(norma);
        
        //wyswietlam ranking stron
        System.out.print("Ranking stron: ");
        PageRank.Ranking1(test.x);
    }
}
