/**
 *  Klasa realizujaca metode iteracji prostej
 */

import java.util.*;

public class IteracjaProsta {
    
    Uklad u;                //uklad do rozwiazania
    int n;                  //rozmiar problemu
    double[] x_st;          //wektor poprzedniego rozwiazania
    double[] x;             //wektor biezacego rozwiazania
    List<Double> norma_x;   //lista norm 
    double[][] D;           //macierz D
    double[] C;             //wektor C
 
      
    //Konstruktor zadjacy uklad wejsciowy
    public IteracjaProsta(Uklad uk) {
        n = uk.n;
        u = new Uklad(uk);
    }
    
    //Metoda obliczajaca macierz D oraz wektor C
    public void Przygotuj(){
        D = new double[n][n];
        C = new double[n];
        x = new double[n];
        x_st = new double[n];
        norma_x = new ArrayList<>();
        for(int i = 0; i < n; i++){
            for(int j = 0; j < n; j++) D[i][j] = - u.A[i][j] / u.A[i][i];
            D[i][i] = 0.0;
            C[i] = u.b[i] / u.A[i][i];
        }
        //System.out.println(NormaWierszowaD());
    }
    
    //Metoda wykonujaca zadana liczbe iteracji, zaczyna od X(0) = C
    //parametr norma - opisany jest ponizej
    public void Iteruj(int iteracje, int norma) {
        int k = 0; 
        System.arraycopy(C, 0, x_st, 0, n);
        norma_x.add(NormaWektora(norma, x_st));
        //System.out.format("%d \t %4.4f\n",k, norma_x.get(k));
        while(k < iteracje){
            for(int i = 0; i < n; i++) {
                x[i] = C[i];
                for(int j = 0; j < n; j++) x[i] += D[i][j] * x_st[j];
            }
            k++;
            norma_x.add(NormaWektora(norma, x));
            //System.out.format("%d \t %4.4f\n",k, norma_x.get(k));
            System.arraycopy(x, 0, x_st, 0, n); 
        }
    }
    
    /*Metoda wykonujaca iteracje do momentu, gdy norma roznicy kolejnych rozwiazan 
     * jest nie wieksza niz eps, zaczyna od X(0) = C */
    //parametr norma jest opisany ponizej
    public void IterujA(double eps, int norma) {
        System.arraycopy(C, 0, x_st, 0, n);
        double blad = 100.0;
        int k = 0;
        norma_x.add(NormaWektora(norma, x_st));
        //System.out.format("%d \t %4.4f\n",k, norma_x.get(k));
        while(blad > eps){
            k++;
            for(int i = 0; i < n; i++) {
                x[i] = C[i];
                for(int j = 0; j < n; j++) x[i] += D[i][j] * x_st[j];
            }
            blad = NormaRoznicy(norma, x, x_st);
            norma_x.add(NormaWektora(norma, x));
            System.out.format("%d \t %4.8f\n",k, norma_x.get(k));
            System.arraycopy(x, 0, x_st, 0, n);    
        }
    }
    
    /*Metoda wykonujaca iteracje do momentu, gdy norma roznicy kolejnych rozwiazan 
     * jest nie wieksza niz eps, zaczyna od zadanego wektora X0 */
    //parametr norma jest opisany ponizej
    public void IterujAWektor(double eps, int norma, double[] X0) {
        System.arraycopy(X0, 0, x_st, 0, n);
        double blad;
        int k = 0;
        blad = 100.0;
        norma_x.add(NormaWektora(norma, x_st));
        System.out.format("%d \t %4.8f\n",k, norma_x.get(k));
        while(blad > eps){
            k++;
            for(int i = 0; i < n; i++) {
                x[i] = C[i];
                for(int j = 0; j < n; j++) x[i] += D[i][j] * x_st[j];
            }
            blad = NormaRoznicy(norma, x, x_st);
            norma_x.add(NormaWektora(norma, x));
            System.out.format("%d \t %4.8f\n",k, norma_x.get(k));
            System.arraycopy(x, 0, x_st, 0, n);    
        }
    }
    
    /*Metoda wykonujaca iteracje wyznaczajc oszacowanie na podstawie twierdzenia
     * Banacha o punkcie stalym, zaczyna od X(0) = 0, wtedy X(1)= C */
    //parametr norma jest opisany ponizej
    public void IterujB(double eps, int norma) {
        int k = 0;
        int N;
        System.arraycopy(C, 0, x_st, 0, n);
        norma_x.add(NormaWektora(norma, x_st));
        System.out.format("%d \t %4.8f\n",k, norma_x.get(k));
        double norma_C = NormaWektora(norma, C);
        double norma_D = NormaMacierzy(norma);
        //obliczam liczbe iteracji wynikajaca z twierdzenia Banacha
        N = (int)Math.ceil(Math.log((eps*(1-norma_D))/norma_C)/Math.log(norma_D) - 1);
        //wykonuje iteracje
        while(k < N){
            for(int i = 0; i < n; i++) {
                x[i] = C[i];
                for(int j = 0; j < n; j++) x[i] += D[i][j] * x_st[j];
            }
            System.arraycopy(x, 0, x_st, 0, n);    
            k++;
            norma_x.add(NormaWektora(norma, x));
            System.out.format("%d \t %4.8f\n",k, norma_x.get(k));
        }
    }
       
    //Metoda wyznaczajaca norme wierszowa macierzy D, przyjmuje parametr rodzaj:
    //0 - norma nieskonczonosc
    //1 - norma kolumnowa
    //2 - norma euklidesowa (pierwiastek sumy kwadratow wspolrzednych - dla uproszczenia,
    //              powinna byc norma spektralna, ale trudniej ja wyznaczyc)
    public double NormaMacierzy(int norma) {
        double norm, suma;
        norm = 0.0;
        switch (norma) {
            case 0: for(int i = 0; i < n; i++) {
                        suma = 0.0;
                        for(int j = 0; j < n; j++) suma += Math.abs(D[i][j]);
                        if(suma > norm) norm = suma;
                    }
                    break;
            case 1: for(int i = 0; i < n; i++) {
                        suma = 0.0;
                        for(int j = 0; j < n; j++) suma += Math.abs(D[j][i]);
                        if(suma > norm) norm = suma;
                    }
                    break;
            case 2: suma = 0.0;
                    for(int i = 0; i < n; i++) {
                        for(int j = 0; j < n; j++) suma += D[i][j] * D[i][j];
                    }
                    norm = Math.sqrt(suma);
                    break;
            }     
        return norm;
    }
    
    //Metoda wyznaczajaca norme wektor, przyjmuje parametr rodzaj:
    //0 - norma nieskonczonosc
    //1 - norma kolumnowa
    //2 - norma euklidesowa
    public double NormaWektora(int norma, double[] y) {
        double norm = 0.0;
        switch (norma) {
            case 0: norm = Math.abs(y[0]);   
                    for(int i = 1; i < y.length; i++) if(Math.abs(y[i]) > norm) norm = Math.abs(y[i]);
                    break;
            case 1: for(int i = 0; i < y.length; i++) norm += Math.abs(y[i]);
                    break;
            case 2: for(int i = 0; i < y.length; i++) norm += y[i] * y[i];
                    norm = Math.sqrt(norm);
                    break;
        }
        return norm;
    }
    
    //Metoda wyznaczajaca norme roznicy dwoch wektorow, argument norma - j.w.
    public double NormaRoznicy(int norma, double[] x1, double[] x2){
        double[] x0 = new double[x1.length];
        for(int i = 0; i < x1.length; i++) x0[i] = x1[i] - x2[i];
        return NormaWektora(norma, x0);
    }
         
    //Metoda wyswietlajaca macierz D oraz wektor C
    public void Wyswietl() {
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) System.out.format("%4.4f\t",D[i][j]);
            System.out.format("\t | \t %4.4f\t",C[i]);
            System.out.print("\n");
        }
    }
    
    //Metoda wypisujaca aktualne rozwiazanie
    public void WyswietlRozwiazanie() {
        for(int i = 0; i < n; i++) System.out.format("%4.8f\t", x[i]);
        System.out.print("\n");
    }
    
    //Metoda sprawdzajaca poprawnosc aktualnego rozwiazania
    public void SprawdzRozwiazanie(int rodzaj){
        u.SprawdzRozwiazanie(x, rodzaj);
    }
}
