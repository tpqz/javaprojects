import java.util.Scanner;

public class SystemyLiczbowe
{
    static void print(String n){
        System.out.println(n);
    }

    public static void main(String[] args)
    {
        print("Podaj liczbę: ");
        Scanner numberIn = new Scanner(System.in);
        String number = numberIn.nextLine();

        print("Podaj podstawę liczbową: ");
        int basis = numberIn.nextInt();

        Integer basisNum = Integer.valueOf(number,basis);
        print("Podana liczba w systemie dziesiętnym: "+ basisNum);
    }


}
