import java.util.Scanner;

public class NowaMetodaStatyczna {

    static void  wyswietlTekst(String s) {

        System.out.print(s);
    }

    static int odczytajLiczbe(){
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }

    static int odczytajLiczbe(String s){
        System.out.print(s);
        Scanner input = new Scanner(System.in);
        return input.nextInt();
    }

    public static void main(String[] args){

        int lpierwsza, ldruga, wynik;

           lpierwsza = odczytajLiczbe("Podaj liczbę pierwszą: ");
            ldruga = odczytajLiczbe("Podaj liczbę drugą: ");
            wynik = lpierwsza + ldruga;
            wyswietlTekst("Suma wynosi: " + wynik + "\n");
    }
}
