import java.util.*;
public class WzorHerona {

    static double addVariable(String s){
        System.out.print(s);
        Scanner input = new Scanner(System.in);
        return input.nextDouble();
    }

    static double formula(double f, double g, double h){
        double p=(f+g+h)/2;
        double S=Math.sqrt(p*(p-f)*(p-g)*(p-h));
        System.out.print(S);
        return S;
    }

    public static void main(String[] args){
        double a, b, c;

        a = addVariable("Podaj parametr a: ");
        b = addVariable("Podaj parametr b: ");
        c = addVariable("Podaj parametr c: ");

        formula(a,b,c);

    }
}
