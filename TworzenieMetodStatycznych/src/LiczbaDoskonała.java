import java.util.Scanner;
import java.lang.Math;

public class LiczbaDoskonała {

    static boolean doskonala(int liczba) {
        int suma = 0;

        for (int i = 1; i <= liczba / 2; i++) {
            if (liczba % i == 0)
                suma += i;
        }

        if (suma == liczba) return true;
         else return false;

    }

    public static void main(String[] args) {


        System.out.println("Podaj liczbę: ");
    Scanner input = new Scanner(System.in);
        int liczba = input.nextInt();


        if(doskonala(liczba)) {
            System.out.print("Liczba " + liczba + " jest doskonała");
        }else{
            System.out.print("Liczba " + liczba + " nie jest doskonała");
        }

    }
}