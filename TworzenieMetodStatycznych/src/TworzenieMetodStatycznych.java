import java.util.Scanner;

public class TworzenieMetodStatycznych {

    static void  wyswietlTekst(String s) {
        System.out.print(s);
    }

    static int odczytajLiczbe(){
       Scanner input = new Scanner(System.in);
        return input.nextInt();
    }


public static void main(String[] args){

    int lpierwsza, ldruga, wynik;

    wyswietlTekst("Podaj pierwsza liczbe: ");
    lpierwsza = odczytajLiczbe();

    wyswietlTekst("Podaj druga liczbe: ");
    ldruga = odczytajLiczbe();

    wynik = lpierwsza + ldruga;

    wyswietlTekst("Suma wynosi: " + wynik + "\n");

    }
}
