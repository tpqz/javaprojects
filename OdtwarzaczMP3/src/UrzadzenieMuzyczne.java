import org.omg.CORBA.ORB;

interface wlacza {
    public void wlacz();
    public void wylacz();
    public boolean czyDziala();
}

interface odtwarza {
    public void play();
    public void pause();
    public void stop();
}


abstract class Odtwarzacz implements wlacza, odtwarza{
    protected int glosnosc;
    private boolean dziala;
    protected String marka;
    private int bateria;
    private int pojemnosc;

        public Odtwarzacz(String marka, int bateria, int pojemnosc){
            this.marka = marka;
            this.dziala = false;
            this.bateria = bateria;
            this.pojemnosc = pojemnosc;
    }

        public void wlacz(){
            dziala = true;
        }

        public void wylacz(){
            dziala = false;
        }

        public boolean czyDziala(){
            return dziala;
        }

    public abstract void glosniej();
    public abstract void ciszej();
}

class OdtwarzaczMP3 extends Odtwarzacz {
    private int bateria;
    private int pojemnosc;

    public OdtwarzaczMP3(String marka, int bateria, int pojemnosc) {
        super(marka, bateria, pojemnosc);
        glosnosc = 10;
        this.bateria = bateria;
        this.pojemnosc = pojemnosc;
    }


    public void glosniej() {
        glosnosc++;
    }

    public void ciszej() {
        glosnosc--;
    }

    public void play() {
        System.out.println("Odtwarzacz MP3 odtwarza muzykę");
    }

    public void stop() {
        System.out.println("Odtwarzacz MP3 nie odtwarza muzyki");
    }

    public void pause() {
        System.out.println("Odtwarzacz MP3 zatrzymany");
    }

    public String toString() {
        return "Odtwarzacz MP3 marki " + marka + "\n" +
                "Odtwarzacz jest " +
                ( czyDziala() ? "włączony" : "wyłączony") + "\n" +
                "Głośność " + glosnosc + "\n" +
                "Pojemność " + pojemnosc + "GB " + "\n" +
                "Stan baterii " + bateria + "%";
    }
}
public class UrzadzenieMuzyczne {

    public static void main (String[] args) {

        OdtwarzaczMP3 odtwarzaczMP3 = new OdtwarzaczMP3("Creative", 78, 16);

        odtwarzaczMP3.wlacz();
        odtwarzaczMP3.glosniej();
        System.out.println(odtwarzaczMP3);
        odtwarzaczMP3.play();
    }
}