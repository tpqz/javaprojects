class Komputer {
    private String marka;
    private Procesor procesor;
    private int pamiec;

        public Komputer(String marka, String procesorMarka, double moc, int pamiec){
            this.marka = marka;
            procesor = new Procesor(procesorMarka, moc);
            this.pamiec = pamiec;
        }

            class Procesor{
                private String markaProcesora;
                private double moc;

                public Procesor(String procesorMarka, double moc){
                    markaProcesora = procesorMarka;
                    this.moc = moc;
                }

                public void rezerwujPamiec(int ilosc){
                    System.out.print("Zarezerwowano " + (pamiec - ilosc) + "MB pamięci");
                }

                public void zwolnijPamiec(int ilosc){
                    System.out.print("Zwolniono " + (pamiec + ilosc) + "MB pamięci" + "\n");
                }

            }
    public void rezerwujPamiec(int ilosc){
        System.out.println("Zarezerwowano " + (ilosc) + "MB pamięci"+ "\n");
    }

    public void zwolnijPamiec(int ilosc){
        System.out.println("Zwolniono " + (ilosc) + "MB pamięci");
    }

    public String toString(){
        return "Komputer marki " + marka + " posiada następujące parametry: " + "\n" +
                "Procesor: " + procesor.markaProcesora + "\n" +
                "Moc obliczeniowa: " + procesor.moc + "GHZ" + "\n" +
                "Pamięć: " + pamiec + "MB";
    }

}

public class Komputery{

    public static void main (String[] args){

        Komputer asus = new Komputer("Asus", "Intel Core 2 Duo", 2.50, 2048);
        System.out.println(asus);
        asus.rezerwujPamiec(1024);

        Komputer lenovo = new Komputer("Lenovo", "Intel Core i7", 3.22, 4096);
        System.out.println(lenovo);
        lenovo.rezerwujPamiec(512);

    }
}