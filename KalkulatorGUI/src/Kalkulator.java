import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


class Container extends JPanel{
    private JButton one, two, three, four, five, six, seven, eight, nine, zero, add, sub, div, mul, dot, equals;
    private boolean start;
    private TextField screen;
    private double result;
    private String lastInput;



    public Container(){

        result = 0;
        lastInput = "=";
        start = true;

        setLayout(new BorderLayout());
        screen = new TextField("0");
        add(screen, BorderLayout.NORTH);

        ActionListener insert = new InsertAction();
        ActionListener operation = new OperationAction();


        JPanel panel = new JPanel();
        GridLayout numbers = new GridLayout(4,4);
        panel.setLayout(numbers);

        one = new JButton("1");
        one.addActionListener(insert);


        two = new JButton("2");
        two.addActionListener(insert);

        three = new JButton("3");
        three.addActionListener(insert);

        four = new JButton("4");
        four.addActionListener(insert);

        five = new JButton("5");
        five.addActionListener(insert);

        six = new JButton("6");
        six.addActionListener(insert);

        seven = new JButton("7");
        seven.addActionListener(insert);

        eight = new JButton("8");
        eight.addActionListener(insert);

        nine = new JButton("9");
        nine.addActionListener(insert);

        zero = new JButton("0");
        zero.addActionListener(insert);

        add = new JButton("+");
        add.addActionListener(operation);

        sub = new JButton("-");
        sub.addActionListener(operation);

        div = new JButton("/");
        div.addActionListener(operation);

        mul = new JButton("*");
        mul.addActionListener(operation);

        dot = new JButton(".");
        dot.addActionListener(insert);

        equals = new JButton("=");
        equals.addActionListener(operation);


                panel.add(one, insert);
                panel.add(two, insert);
                panel.add(three, insert);
                panel.add(div, operation);

                panel.add(four, insert);
                panel.add(five, insert);
                panel.add(six, insert);
                panel.add(mul, operation);

                panel.add(seven, insert);
                panel.add(eight, insert);
                panel.add(nine, insert);
                panel.add(add, operation);

                panel.add(zero, insert);
                panel.add(dot, insert);
                panel.add(sub, operation);
                panel.add(equals, operation);

        add(panel, BorderLayout.CENTER);
    }

    private class InsertAction implements ActionListener{
        public void actionPerformed(ActionEvent event) {
            String insert = event.getActionCommand();
            if (start){
                start = false;
                screen.setText("");
            }
            screen.setText(screen.getText() + insert);
        }
    }

    private class OperationAction implements ActionListener{
        public void actionPerformed(ActionEvent event) {
            String operation = event.getActionCommand();
            if (start){
                if(operation.equals("-")) {
                    screen.setText(operation);
                    start = false;
                }
                else lastInput = operation;
            }
            else{
                calculate(Double.parseDouble(screen.getText()));
                lastInput = operation;
                start = true;
            }
        }
    }

    public void calculate(double a){
        if (lastInput.equals("+")) result += a;
        else if (lastInput.equals("-")) result -= a;
        else if (lastInput.equals("/")) result /= a;
        else if (lastInput.equals("*")) result *= a;
        else if (lastInput.equals("=")) result = a;
        screen.setText("" + result);
    }
}

class Window extends JFrame {

    public Window(){
        setTitle("Kalkulator");
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(250,250);
        setResizable(true);
    }
}

public class Kalkulator {
    public static void main(String[] args){

        Window window = new Window();
        Container container = new Container();
        window.add(container);
    }
}
