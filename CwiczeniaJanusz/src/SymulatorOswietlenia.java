import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class SymulatorOswietlenia extends JFrame {
    private JButton on, off;
    private JPanel przyciski, oswietlenie;
    private JLabel stan;
    private boolean choice;

    public SymulatorOswietlenia() {

        setTitle("SymulatorOswietlenia");
        setVisible(true);
        setSize(300, 200);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        stan = new JLabel("Ciemność");
        przyciski = new JPanel();
        oswietlenie = new JPanel();

        on = new JButton("Włącz");
        off = new JButton("Wyłącz");


        Obsluga bON = new Obsluga(Color.yellow, "Oswietlenie włączone", choice);
        on.addActionListener(bON);

        Obsluga bOFF = new Obsluga(Color.black, "Oswietlenie wyłączone", choice);
        off.addActionListener(bOFF);

        przyciski.add(on);
        przyciski.add(off);

        oswietlenie.setBackground(Color.black);

        add(przyciski, BorderLayout.NORTH);
        add(oswietlenie, BorderLayout.CENTER);
        add(stan, BorderLayout.SOUTH);
    }

    private class Obsluga implements ActionListener {
        private Color kolor;
        private String napis;
        private boolean choice;

        public Obsluga(Color c, String n, boolean ch) {
            this.kolor = c;
            this.napis = n;
            this.choice = ch;
        }

        public void actionPerformed(ActionEvent e) {
            oswietlenie.setBackground(kolor);
            stan.setText("Aktualny stan: " + napis);
        }
    }


    public static void main (String[] args){
        SymulatorOswietlenia okno = new SymulatorOswietlenia();

    }
}
