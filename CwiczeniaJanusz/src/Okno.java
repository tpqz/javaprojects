import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Okno extends JFrame {
   /*
    public Okno() {
        setTitle("Moje okno");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 300);
        setVisible(true);

    }

    public Okno() {
        setTitle("Model RGB przestrzeni barw");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(400, 200);
        JPanel kontener = new JPanel();
        kontener.add(new JButton("R-czerwony"));
        kontener.add(new JButton("G-zielony"));
        kontener.add(new JButton("B-niebieski"));
        add(kontener, BorderLayout.SOUTH);
        add(new JTextArea("Paleta barw..."), BorderLayout.CENTER);
        setVisible(true);
    }



    public Okno() {
        JButton b = new JButton("Przycisk");
        Handler h = new Handler();
        b.addActionListener(h);
        add(b);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
    }
*/
    JPanel kontener = new JPanel();
    JTextField t = new JTextField();
    JButton a = new JButton("DODAJ");
    JButton b = new JButton("RESET");
    GridLayout experimentLayout = new GridLayout(0,2);
    public Okno() {
        Handler h = new Handler();
        Handler2 h2 = new Handler2();

        a.addActionListener(h2);
        b.addActionListener(h);

        kontener.setLayout(experimentLayout);
        add(t, BorderLayout.PAGE_START);
        kontener.add(b, BorderLayout.LINE_END);
        kontener.add(a, BorderLayout.PAGE_START);
        add(kontener, BorderLayout.SOUTH);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(300,200);
        setVisible(true);
    }

    private class Handler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            t.setText(" ");
        }
    }

    private class Handler2 implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            System.out.println(t.getText());
        }
    }

    public boolean blokujRozmiar(boolean rozmiar){
        setResizable(rozmiar);
        return rozmiar;
    }

    public void wysrodkuj(){
        setLocation(500, 250);
    }

    public void doKrawedzi(int x) {
        switch (x) {
            case 1:
                setLocation(1,1);
                break;
            case 2:
                setLocation(1,768-300);
                break;
            case 3:
                setLocation(1366-400,1);
                break;
            case 4:
                setLocation(1366-400,768-300);
                break;
        }
    }


    public static void main(String[] args) {

        Okno okno = new Okno();


    }
}
