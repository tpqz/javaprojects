import java.util.Scanner;

public class SystemyLiczbowe {

    public static long hex2dec(String hex) {
        Long hexNum = Long.valueOf(hex, 16);
        System.out.println(hex + "(16)" + " = " + hexNum + "(10)" + "\n" + "------------------" +
                                                                            "-------------------");
        if (hexNum == 0) {
            System.out.println("Program zakonczył pracę");
            System.exit(0);
        }else {
            return hexNum;
        }
        return hexNum;
    }

    public static void main(String[] args) {

        String input = "";
        System.out.println("--Konwerter liczb szesnastkowych na system dziesiętny--");
        System.out.println("Aby zakończyć pracę wpisz liczbę 0");
        System.out.println("Maksymalna wartość: 7fffffffffffffff" + "\n");
        Scanner in = new Scanner(System.in);

            do {
                try {
                    System.out.print("Podaj liczbe w systemie szesnastkowym: ");
                    input = in.nextLine();
                    hex2dec(input);
                } catch (java.lang.NumberFormatException a) {
                    System.out.println("Wprowadziłeś/aś nieprawidłową liczbę!" + "\n" + "-------------------" +
                                                                                         "------------------");
                }
            } while (input != null);
    }
}


