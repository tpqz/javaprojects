import java.util.Scanner;

public class PismoRozstrzelone {

    public static void main(String[] args){

        System.out.print("Podaj zdanie: ");
        Scanner in = new Scanner(System.in);
        String zdanie = in.nextLine();

        for (int i = 0; i < zdanie.length(); i++){

                if (i == zdanie.length() - 1) {
                    System.out.print(zdanie.charAt(i));
                }else{
                    System.out.print(zdanie.charAt(i) + " ");
                }
        }
    }
}
