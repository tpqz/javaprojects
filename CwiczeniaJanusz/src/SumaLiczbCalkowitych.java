import java.util.*;
public class SumaLiczbCalkowitych {
    public static void main(String[] args) {
        int sumaLiczb = 0;
        int liczbaCalkowita = 0;
        System.out.println("Wprowadź liczby całkowite.");
        System.out.println("Wartość 0 kończy wprowadzanie.\n");
        Scanner sc = new Scanner(System.in);
        do {
            try {
                System.out.print("Liczba całkowita: ");
                liczbaCalkowita = sc.nextInt();
                sumaLiczb += liczbaCalkowita;
            }
            catch (InputMismatchException e) {
                System.out.println("Wprowadzona wartość \""
                        + sc.nextLine() + "\" jest niepoprawna!!!");
            }
        } while (liczbaCalkowita!=0);
        System.out.println("Suma wprowadzonych wartości: " + sumaLiczb);
    }
}