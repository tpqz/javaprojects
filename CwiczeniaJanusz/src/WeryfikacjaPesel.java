import java.util.Scanner;

public class WeryfikacjaPesel {

    public static void main(String[] args) {

        System.out.println("Podaj swój pesel: ");
        Scanner in = new Scanner(System.in);
        String pesel = in.nextLine();

        if (pesel.length() < 11){
            System.out.println("Niepoprawny numer pesel");
        }else{
            int[] peselNum = new int[pesel.length()];

            for (int i = 0; i < pesel.length(); i++){
                Character c = pesel.charAt(i);
                peselNum[i] = (int) c.charValue();
            }
            int wynik =
                peselNum[0] * 1 +
                peselNum[1] * 3 +
                peselNum[2] * 7 +
                peselNum[3] * 9 +
                peselNum[4] * 1 +
                peselNum[5] * 3 +
                peselNum[6] * 7 +
                peselNum[7] * 9 +
                peselNum[8] * 1 +
                peselNum[9] * 3 +
                    peselNum[10];

            int kontrolna = wynik%10;

            if (kontrolna == 0){
                System.out.println("Pesel poprawny");
            }else{
                System.out.println("Niepoprawny numer pesel");
            }
        }



    }
}

