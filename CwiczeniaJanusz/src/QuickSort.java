import java.util.Scanner;
import java.util.Arrays;

public class QuickSort {
    public static void main(String[] args){

        System.out.println("Podaj dolna granice: ");
        Scanner in = new Scanner(System.in);
        int dolna = in.nextInt();

        System.out.println("Podaj gorna granice: ");
        int gorna = in.nextInt();

        System.out.println("Podaj ilość liczb: ");
        int ilosc = in.nextInt();

        long[] przedzial = new long[ilosc];

        for (int i = 0; i <= ilosc; i++){
            przedzial[i] = dolna + (int) Math.random()*(gorna-dolna+1);
        }
    }
}