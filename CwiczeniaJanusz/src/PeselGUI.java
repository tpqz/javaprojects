import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Container extends JPanel {
    private TextField peselArea;
    private JLabel tekst;
    private JButton button;

    public Container() {
        peselArea = new TextField(35);
        tekst = new JLabel("Podaj pesel: ");
        button = new JButton("Wyznacz plec");

        Events hitButton = new Events();
        button.addActionListener(hitButton);

        add(tekst);
        add(peselArea);
        add(button, BorderLayout.PAGE_END);
    }

    private class Events implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            String pesel = peselArea.getText();

            if (pesel.length() == 11 && pesel.charAt(9) % 2 == 0) {
                JOptionPane.showMessageDialog(null, "Kobieta");
            } else if (pesel.length() == 11 && pesel.charAt(9) % 2 != 0) {
                JOptionPane.showMessageDialog(null, "Mezczyzna");
            } else if (pesel.length() != 11) {
                JOptionPane.showMessageDialog(null, "Podaj poprawny pesel");
            }
        }
    }
}

class Window extends JFrame {
    public Window() {

        setTitle("Program wyznaczający płeć");
        setSize(400,100);
        setVisible(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
}

public class PeselGUI{

    public static void main(String[] args){

        Window window = new Window();
        Container container = new Container();
        window.add(container);

    }
}
