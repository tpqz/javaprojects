import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


class Handler implements ActionListener {
    public void actionPerformed(ActionEvent e) {
        System.out.println("Przycisk został naciśnięty!");
    }
}
public class ObslugaZdarzen extends JFrame {
    public ObslugaZdarzen() {
        JButton b = new JButton("Przycisk");
        Handler h = new Handler();
        b.addActionListener(h);
        add(b);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
    }
}