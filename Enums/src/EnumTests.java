/**
 * Created by Mateusz on 28.04.2017.
 */
public class EnumTests {
    public enum Kolor {
        NIEBIESKI(false),
        CZERWONY(false),
        BIALY(false);

        boolean ladny;

        private Kolor(boolean czyLadny){
            ladny = czyLadny;
        }
    }

    public static void main(String [] argv) {
        Kolor test = EnumTests.Kolor.CZERWONY;
        String text = "test";
        System.out.println(text.substring(0,1));
    }
}
